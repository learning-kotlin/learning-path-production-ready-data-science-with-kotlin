package org.elu.learning.kotlin.datascience

fun main(args: Array<String>) {
    // demo
    println("Hello World!")

    // read from file
    readFromFile()

    // read from file with buffer
    readFromFileWithBuffer()

    // read into data class
    readFromFileIntoDataClass()

    // read from database
    println()
    println("===== from database ======")
    val orders = readFromDatabase()
    orders.forEach {
        println(it)
    }

    // read from database with parameters
    println()
    println("===== orders for customer id 5 from database ======")
    val customerOrders = ordersForCustomerId(5)
    customerOrders.forEach { println(it) }

    // read from web
    println()
    println("===== from web ======")
    readFromWebRequest()

    // read from web with lazy loading
    println()
    println("===== lazy loading, property isn't loaded yet ======")
    println(usStates)
}
