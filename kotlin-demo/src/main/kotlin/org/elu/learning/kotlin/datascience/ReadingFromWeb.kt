package org.elu.learning.kotlin.datascience

import java.net.URL

fun readFromWebRequest() {
    val usaStates = URL("https://goo.gl/S0xuOi").readText().split(Regex("\\r?\\n"))

    println(usaStates)
}

val usStates by lazy {
    println("property called, loading and caching!")
    URL("https://goo.gl/S0xuOi").readText().split(Regex("\\r?\\n"))
}
