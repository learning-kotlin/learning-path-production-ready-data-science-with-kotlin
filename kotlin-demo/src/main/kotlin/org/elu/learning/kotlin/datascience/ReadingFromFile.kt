package org.elu.learning.kotlin.datascience

import java.io.File
import java.time.LocalDate

data class CustomerOrder(
        val customerOrderId: Int,
        val customerId: Int,
        val orderDate: LocalDate,
        val productId: Int,
        val quantity: Int
)

// reading from file
fun readFromFile() {
    val lines = File("../resources/customer_orders.csv").readLines()

    // iterative style
    for (l in lines) {
        println(l)
    }

    // functional style
    lines.forEach {
        println(it)
    }
}

fun readFromFileWithBuffer() {
    val reader = File("../resources/customer_orders.csv").bufferedReader()
    reader.forEachLine {
        println(it)
    }
}

fun readFromFileIntoDataClass() {
    val reader = File("../resources/customer_orders.csv").bufferedReader()

    val orders = reader.readLines()
            .drop(1)
            .map { it.split(",") }
            .map {
                CustomerOrder(
                        customerOrderId = it[0].toInt(),
                        customerId = it[1].toInt(),
                        orderDate = LocalDate.parse(it[2]),
                        productId = it[3].toInt(),
                        quantity = it[4].toInt()
                )
            }

    orders.forEach {
        println(it)
    }
}
