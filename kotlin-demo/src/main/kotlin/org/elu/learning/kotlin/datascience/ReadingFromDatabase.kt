package org.elu.learning.kotlin.datascience

import java.sql.DriverManager
import java.time.LocalDate

fun readFromDatabase(): List<CustomerOrder> {
    val conn = DriverManager.getConnection("jdbc:sqlite:../resources/thunderbird_manufacturing.db")

    val ps = conn.prepareStatement("SELECT * FROM CUSTOMER_ORDER")
    val rs = ps.executeQuery()

    val orders = mutableListOf<CustomerOrder>()

    while (rs.next()) {
        orders += CustomerOrder(
                rs.getInt("CUSTOMER_ORDER_ID"),
                rs.getInt("CUSTOMER_ID"),
                LocalDate.parse(rs.getString("ORDER_DATE")),
                rs.getInt("PRODUCT_ID"),
                rs.getInt("QUANTITY")
        )
    }

    ps.close()
    return orders
}

fun ordersForCustomerId(customerId: Int): List<CustomerOrder> {
    val conn = DriverManager.getConnection("jdbc:sqlite:../resources/thunderbird_manufacturing.db")

    val ps = conn.prepareStatement("SELECT * FROM CUSTOMER_ORDER WHERE CUSTOMER_ID = ?")
    ps.setInt(1, customerId)
    val rs = ps.executeQuery()

    val orders = mutableListOf<CustomerOrder>()
    while (rs.next()) {
        orders += CustomerOrder(
                rs.getInt("CUSTOMER_ORDER_ID"),
                rs.getInt("CUSTOMER_ID"),
                LocalDate.parse(rs.getString("ORDER_DATE")),
                rs.getInt("PRODUCT_ID"),
                rs.getInt("QUANTITY")
        )
    }

    ps.close()
    return orders
}
