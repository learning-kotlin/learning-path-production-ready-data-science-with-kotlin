package org.elu.learning.kotlin.datascience

import java.time.DayOfWeek
import java.time.LocalDate

fun sampleExtensionFunction() {
    val myDate = LocalDate.of(2018, 6, 5)

    val weekStartDate = myDate.startOfWeek()

    println(weekStartDate)

    println(myDate.startOfWeek(DayOfWeek.FRIDAY))

    println(myDate.startOfWeekProp)
}

// simple extension function
fun LocalDate.startOfWeek() = (0..6).asSequence()
        .map { this.minusDays(it.toLong()) }
        .first { it.dayOfWeek == DayOfWeek.MONDAY }

// extension function with parameters
fun LocalDate.startOfWeek(startDayOfWeek: DayOfWeek = DayOfWeek.MONDAY) = (0..6).asSequence()
        .map { this.minusDays(it.toLong()) }
        .first { it.dayOfWeek == startDayOfWeek }

// extension function as object property
val LocalDate.startOfWeekProp get() = (0..6).asSequence()
        .map { this.minusDays(it.toLong()) }
        .first { it.dayOfWeek == DayOfWeek.MONDAY }
