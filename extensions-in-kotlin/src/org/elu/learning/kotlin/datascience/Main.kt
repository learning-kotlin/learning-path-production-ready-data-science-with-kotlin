package org.elu.learning.kotlin.datascience

fun main(args: Array<String>) {
    println("==== Extension Functions ====")
    sampleExtensionFunction()

    println("==== Extension operators ====")
    extensionOperators()

    println("==== Leveraging DSL ====")
    sampleDSL()
}
