package org.elu.learning.kotlin.datascience

import java.time.LocalDate

fun extensionOperators() {
    val myDate = LocalDate.of(2018, 4, 22)

    val tomorrow = myDate + 1
    println(tomorrow)

    val yesterday = myDate - 1
    println(yesterday)
}

operator fun LocalDate.plus(days: Int) = plusDays(days.toLong())
operator fun LocalDate.minus(days: Int) = minusDays(days.toLong())