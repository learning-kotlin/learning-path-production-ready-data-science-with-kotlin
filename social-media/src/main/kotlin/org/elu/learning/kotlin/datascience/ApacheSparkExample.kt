package org.elu.learning.kotlin.datascience

import org.apache.spark.SparkConf
import org.apache.spark.api.java.JavaSparkContext
import kotlin.reflect.KClass

fun sampleApacheSpark() {
    val conf = SparkConf()
            .setMaster("local") // use local machine as master
            .setAppName("Kotlin Spark Test")

    conf.registerKryoClasses(MyItem::class)

    val sc = JavaSparkContext(conf)

    val items = listOf("123/643/7563/2134/ALPHA", "2343/6356/BETA/2342/12", "23423/656/343")

    val input = sc.parallelize(items)

    val sumOfNumbers = input.flatMap{ it.split("/").iterator() }
            .filter { it.matches(Regex("[0-9]+")) }
            .map { it.toInt() }
            .reduce { total, next -> total + next }

    println(sumOfNumbers)

    println("==== second example ====")
    val items2 = listOf(MyItem(1,"Alpha"), MyItem(2,"Beta"))

    val input2 = sc.parallelize(items2)

    val letters = input2.flatMap { it.value.split(Regex("(?<=.)")).iterator() }
            .map(String::toUpperCase)
            .filter { it.matches(Regex("[A-Z]")) }

    println(letters.collect())
}

class MyItem(val id: Int, val value: String)

// extension function to register Kotlin classes
private fun SparkConf.registerKryoClasses(vararg args: KClass<*>) = registerKryoClasses(args.map { it.java }.toTypedArray())
