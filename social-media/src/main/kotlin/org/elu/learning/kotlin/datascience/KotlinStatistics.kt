package org.elu.learning.kotlin.datascience

import org.nield.kotlinstatistics.percentileBy
import org.nield.kotlinstatistics.standardDeviation
import org.nield.kotlinstatistics.standardDeviationBy
import java.time.LocalDate
import java.time.Month

data class Patient(val firstName: String,
                   val lastName: String,
                   val gender: Gender,
                   val birthday: LocalDate,
                   val whiteBloodCellCount: Int)

enum class Gender {
    MALE,
    FEMALE
}

val patients = listOf(
        Patient("John", "Simone", Gender.MALE, LocalDate.of(1989, 1, 7), 4500),
        Patient("Sarah", "Marley", Gender.FEMALE, LocalDate.of(1970, 2, 5), 6700),
        Patient("Jessica", "Arnold", Gender.FEMALE, LocalDate.of(1980, 3, 9), 3400),
        Patient("Sam", "Beasley", Gender.MALE, LocalDate.of(1981, 4, 17), 8800),
        Patient("Dan", "Forney", Gender.MALE, LocalDate.of(1985, 9, 13), 5400),
        Patient("Lauren", "Michaels", Gender.FEMALE, LocalDate.of(1975, 8, 21), 5000),
        Patient("Michael", "Erlich", Gender.MALE, LocalDate.of(1985, 12, 17), 4100),
        Patient("Jason", "Miles", Gender.MALE, LocalDate.of(1991, 11, 1), 3900),
        Patient("Rebekah", "Earley", Gender.FEMALE, LocalDate.of(1985, 2, 18), 4600),
        Patient("James", "Larson", Gender.MALE, LocalDate.of(1974, 4, 10), 5100),
        Patient("Dan", "Ulrech", Gender.MALE, LocalDate.of(1991, 7, 11), 6000),
        Patient("Heather", "Eisner", Gender.FEMALE, LocalDate.of(1994, 3, 6), 6000),
        Patient("Jasper", "Martin", Gender.MALE, LocalDate.of(1971, 7, 1), 6000)
)

data class GenderAndMonth(val gender: Gender, val month: Month)

fun sampleKotlinStatistics() {
    val averageWbcc = patients.map { it.whiteBloodCellCount }.average()

    val standardDevWbcc = patients.map { it.whiteBloodCellCount }.standardDeviation()

    println("Average WBCC: $averageWbcc, Std Dev WBCC: $standardDevWbcc")
    println()

    val standardDevByGender = patients.standardDeviationBy(
            keySelector = { it.gender },
            valueSelector = { it.whiteBloodCellCount }
    )
    println(standardDevByGender)
    println()

    val standardDevByGenderAndMonth = patients.standardDeviationBy(
            keySelector = { GenderAndMonth(it.gender, it.birthday.month) },
            valueSelector = { it.whiteBloodCellCount }
    )
    standardDevByGenderAndMonth.forEach { println(it) }
    println()

    fun Collection<Patient>.wbccPercentileByGender(percentile: Double) =
            percentileBy(
                    percentile = percentile,
                    keySelector = { it.gender },
                    valueSelector = { it.whiteBloodCellCount.toDouble() }
            )

    val percentileQuadrantsByGender = patients.let {
        mapOf(1.0 to it.wbccPercentileByGender(1.0),
                25.0 to it.wbccPercentileByGender(25.0),
                50.0 to it.wbccPercentileByGender(50.0),
                75.0 to it.wbccPercentileByGender(75.0),
                100.0 to it.wbccPercentileByGender(100.0)
        )
    }
    percentileQuadrantsByGender.forEach(::println)
    println()
}
