package org.elu.learning.kotlin.datascience

fun main(args: Array<String>) {
    //Retrieve users "John" and "Billy"
    val user1 = users.first { it.firstName == "John" }
    val user2 = users.first { it.firstName == "Billy" }

    // see mutual friend for John and Billy
    user1.mutualFriendsOf(user2).forEach { println(it) }
    println()

    // see recommended friends for John
    user1.recommendedFriends().forEach { println(it) }
    println()

    // Apache Spark example
    println("==== Apache Spark sample ====")
    sampleApacheSpark()

    // Kotlin statistics
    println("==== Kotlin statistics ====")
    sampleKotlinStatistics()

    // ND4J (NumPy for Java)
    println("==== ND4J ====")
    sampleND4J()
}
