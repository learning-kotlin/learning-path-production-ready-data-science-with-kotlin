package org.elu.learning.kotlin.datascience

fun combineStrings(str1: String, str2: String, combiner: (String, String) -> String) = combiner(str1, str2)