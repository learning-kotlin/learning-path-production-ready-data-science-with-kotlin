package org.elu.learning.kotlin.datascience

fun mapStringToInt(input: String, mapper: (String) -> Int) = mapper(input)

inline fun mapStringToIntInline(input: String, crossinline mapper: (String) -> Int) = mapper(input)
