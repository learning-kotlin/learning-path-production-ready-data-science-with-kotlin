package org.elu.learning.kotlin.datascience

fun simpleSequence() {
    val codeWords = sequenceOf("Alpha", "Bravo", "Charlie", "Delta", "Echo", "Foxtrot")

    val averageLength = codeWords.map { it.length }.average()
    println(averageLength)
}

fun complexSequenceExample() {
    val codeWords = listOf("Alpha", "Bravo", "Charlie", "Delta", "Echo", "Foxtrot")

    val distinctLetters = codeWords.asSequence()
            .flatMap { it.toCharArray().asSequence() }
            .map { it.toUpperCase() }
            .distinct()
            .toList()

    println(distinctLetters)
}
