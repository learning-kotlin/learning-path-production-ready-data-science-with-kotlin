package org.elu.learning.kotlin.datascience

import java.time.LocalDate
import java.time.LocalTime

fun simpleTraditionalExample() {
    val myString = "Bravo"

    val uppercased = myString.toUpperCase()

    val mirrored = uppercased + "|" + uppercased.reversed()

    println(mirrored)
}

fun sameWithLet() {
    val myString = "Bravo"

    val mirrored = myString.toUpperCase().let { it + "|" + it.reversed() }

    println(mirrored)
}

fun letWithSequences() {
    val codeWords = sequenceOf("Alpha", "Bravo", "Charlie", "Delta", "Echo", "Foxtrot")

    codeWords.flatMap { it.toCharArray().asSequence() }
            .map { it.toUpperCase() }
            .distinct()
            .joinToString(",")
            .let { println(it) }
}

fun sampleApply() {
    val todaysDate = LocalDate.now().apply {
        println("Constructed today's date, which is $month $dayOfMonth, $year")
    }
}

class MyProcess {
    init {
        println("Constructing MyProcess")
    }

    fun run() {
        println("Starting process at ${LocalTime.now()}")
    }
}

fun processSample() {
    val immediateProcess = MyProcess().apply {
        run()
    }
}
