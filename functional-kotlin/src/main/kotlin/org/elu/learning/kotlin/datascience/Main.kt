package org.elu.learning.kotlin.datascience

fun main(args: Array<String>) {
    // higher order functions
    println("==== Higher order functions ====")
    val myString = "Foxtrot"
    val length = mapStringToInt(input = myString, mapper = { it.length })
    println(length)

    // simplified
    val length2 = mapStringToInt(myString) { it.length }
    println(length2)

    // with inline more efficient
    val length3 = mapStringToIntInline(myString) { it.length }
    println(length3)

    // Lambdas
    println("==== Lambdas ====")
    println(mapStringToInt(myString) { s -> s.length })

    // using reflection syntax
    println(mapStringToInt(myString, String::length))

    val str1 = "Alpha"
    val str2 = "Beta"
    val combineMethod1 = combineStrings(str1, str2) { s1, s2 -> s1 + s2 }
    println(combineMethod1)

    val combineMethod2 = combineStrings(str1, str2) { s1, s2 -> "$s1-$s2" }
    println(combineMethod2)

    // Generics
    println("==== Generics ====")
    genericsSample()

    genericInputSample()

    // Sequences
    println("==== Sequences ====")
    simpleSequence()

    complexSequenceExample()

    // let and apply
    println("==== let and apply ====")
    simpleTraditionalExample()
    sameWithLet()
    letWithSequences()

    sampleApply()
    processSample()
}
