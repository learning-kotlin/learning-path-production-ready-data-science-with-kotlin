package org.elu.learning.kotlin.datascience

import java.time.LocalDate

fun genericsSample() {
    val dateString = "2108-01-01"

    val date = mapString(dateString) { LocalDate.parse(it) }
    println(date)

    val length = mapString(dateString) { it.length }
    println(length)
}

inline fun <R> mapString(input: String, crossinline mapper: (String) -> R) = mapper(input)

fun genericInputSample() {
    val dateString = "2108-01-01"

    val date = map(dateString) { LocalDate.parse(it) }
    println(date)

    val myNumber = 5
    val myNumberAsString = map(myNumber) {
        when (it) {
            1 -> "ONE"
            2 -> "TWO"
            3 -> "THREE"
            4 -> "FOUR"
            5 -> "FIVE"
            6 -> "SIX"
            7 -> "SEVEN"
            8 -> "EIGHT"
            9 -> "NINE"
            else -> throw Exception("Only 1 through 9 supported")
        }
    }
    println(myNumberAsString)
}

inline fun <T, R> map(input: T, crossinline mapper: (T) -> R) = mapper(input)
